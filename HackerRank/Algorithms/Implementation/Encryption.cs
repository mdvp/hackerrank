﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HackerRank.Algorithms.Implementation
{
    class Encryption
    {
        static void Main(String[] args)
        {
            var input = Console.ReadLine().Trim();
            var length = input.Length;

            var sqrt = Math.Sqrt(length);
            var min = Math.Floor(sqrt);
            var max = Math.Ceiling(sqrt);
            var stack = new Queue<char>(input);

            var x = min;
            var y = min;

            if (x * y < length)
                y = max;

            if (x * y < length)
                x = max;

            var matrix = new char[(int)y, (int)x];

            for (var c = 0; c < x; c++)
                for (var r = 0; r < y; r++)
                    matrix[r, c] = (stack.Any()) ? stack.Dequeue() : ' ';

            var output = "";

            for (var r = 0; r < y; r++)
            {
                for (var c = 0; c < x; c++)
                {
                    output += matrix[r, c];
                }

                output = output.Trim() + " ";
            }

            Console.WriteLine(output.Trim());
        }
    }
}
