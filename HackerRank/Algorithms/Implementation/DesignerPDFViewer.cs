﻿using System;
using System.Linq;

namespace HackerRank.Algorithms.Implementation
{
    class DesignerPDFViewer
    {
        static void Main(String[] args)
        {
            var abc = new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            var h_temp = Console.ReadLine().Split(' ');
            var h = Array.ConvertAll(h_temp, byte.Parse);
            var word = Console.ReadLine();
            var width = word.Length;
            var height = word.Max(x => h[Array.IndexOf(abc, x)]);
            var area = width * height;

            Console.WriteLine(area);
        }
    }
}
