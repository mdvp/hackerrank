﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Implementation
{
    class Kangaroo
    {
        static void Main(String[] args)
        {
            string[] tokens_x1 = Console.ReadLine().Split(' ');
            int x1 = Convert.ToInt32(tokens_x1[0]);
            int v1 = Convert.ToInt32(tokens_x1[1]);
            int x2 = Convert.ToInt32(tokens_x1[2]);
            int v2 = Convert.ToInt32(tokens_x1[3]);

            var jumps = 1;
            var isOk = false;

            while (jumps <= 10000)
            {
                x1 += v1;
                x2 += v2;

                if (x1 == x2)
                {
                    isOk = true;
                    break;
                }

                jumps++;
            }

            Console.WriteLine((isOk ? "YES" : "NO"));
        }
    }
}
