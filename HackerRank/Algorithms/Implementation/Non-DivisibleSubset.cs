﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Implementation
{
    class Non_DivisibleSubset
    {
        static void Main(String[] args)
        {
            /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */

            var i_temp = Console.ReadLine().Split(' ');
            var n = Convert.ToInt32(i_temp[0]);
            var k = Convert.ToInt32(i_temp[1]);
            var arr_temp = Console.ReadLine().Split(' ');
            var arr = Array.ConvertAll(arr_temp, Int64.Parse);

            var r = new int[k];

            for (var i = 0; i < n; i++)
                r[arr[i] % k] += 1;

            var res = 0;

            for (var i = 0; i < k / 2 + 1; i++)
            {
                if (i == 0 || k == i * 2)
                    res += (r[i] != 0) ? 1 : 0;
                else
                    res += Math.Max(r[i], r[k - i]);
            }

            Console.WriteLine(res);
        }
    }
}
