﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Implementation
{
    class CutTheSticks
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] arr_temp = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll(arr_temp, Int32.Parse);

            var nSticks = n;

            while (nSticks > 0)
            {
                Console.WriteLine(nSticks);

                var minPos = 1000;

                for (int i = 0; i < n; i++)
                {
                    if (arr[i] < minPos && arr[i] > 0)
                        minPos = arr[i];
                }

                for (int i = 0; i < n; i++)
                {
                    arr[i] = arr[i] - minPos;

                    if (arr[i] == 0)
                        nSticks--;
                }
            }
        }
    }
}
