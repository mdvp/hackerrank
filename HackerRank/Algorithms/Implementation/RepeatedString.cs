﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Implementation
{
    class RepeatedString
    {
        static void Main(String[] args)
        {
            string s = Console.ReadLine();
            long n = Convert.ToInt64(Console.ReadLine());

            var a = s.Count(i => i == 'a') * (n / s.Length);
            var l = s.Length * (n / s.Length);
            var r = n % l;

            Console.WriteLine(a + s.Substring(0, Convert.ToInt32(r)).Count(i => i == 'a'));
        }
    }
}
