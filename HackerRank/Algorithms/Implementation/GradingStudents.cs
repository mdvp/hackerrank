﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HackerRank.Algorithms.Implementation
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/grading/problem
    /// </summary>
    class GradingStudents
    {
        static int[] solve(int[] grades)
        {
            // Complete this function
            var result = new List<int>();

            foreach (var grade in grades)
            {
                if (grade < 0 || grade > 100)
                    continue;

                if (grade >= 38)
                {
                    var multipleFive = (int)(Math.Ceiling((double)grade / (double)5) * 5.0);

                    if (multipleFive - grade < 3)
                    {
                        result.Add(multipleFive);
                        continue;
                    }
                }

                result.Add(grade);
            }

            return result.ToArray();
        }

        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[] grades = new int[n];
            for (int grades_i = 0; grades_i < n; grades_i++)
            {
                grades[grades_i] = Convert.ToInt32(Console.ReadLine());
            }
            int[] result = solve(grades);
            Console.WriteLine(String.Join("\n", result));


        }
    }
}
