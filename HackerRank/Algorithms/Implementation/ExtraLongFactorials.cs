﻿using System;
using System.Numerics;

namespace HackerRank.Algorithms.Implementation
{
    class ExtraLongFactorials
    {
        static void Main(String[] args)
        {
            var n = Convert.ToByte(Console.ReadLine());
          
            Console.WriteLine(GetFactorial(n));
        }

        private static BigInteger GetFactorial(byte input)
        {
            if (input <= 1)
                return 1;

            return input * GetFactorial((byte)(input - 1));
        }
    }
}
