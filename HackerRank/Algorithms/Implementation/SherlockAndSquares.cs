﻿using System;

namespace HackerRank.Algorithms.Implementation
{
    class SherlockAndSquares
    {
        static void Main(String[] args)
        {
            var t = Convert.ToByte(Console.ReadLine());

            for (var i = 0; i < t; i++)
            {
                var input = Array.ConvertAll(Console.ReadLine().Split(' '), ulong.Parse);
                var a = input[0];
                var b = input[1];

                var s = (int)Math.Sqrt(b) - (int)Math.Sqrt(a - 1);

                Console.WriteLine(s);
            }
        }
    }
}
