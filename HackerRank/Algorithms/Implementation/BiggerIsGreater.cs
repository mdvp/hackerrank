﻿using System;
using System.Linq;

namespace HackerRank.Algorithms.Implementation
{
    class BiggerIsGreater
    {
        static void Main(String[] args)
        {
            var abc = new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            var t = Convert.ToUInt32(Console.ReadLine());

            for (var i = 0; i < t; i++)
            {
                var word = Console.ReadLine();
                var indexes = word.Select(x => Array.IndexOf(abc, x)).ToArray();
                var hasPermutation = NextPermutation(indexes);
                var result = indexes.Select(x => abc[x]);

                if (hasPermutation)
                    Console.WriteLine(string.Join("", result));
                else
                    Console.WriteLine("no answer");
            }
        }

        static bool NextPermutation(int[] array)
        {
            // Find non-increasing suffix
            var i = array.Length - 1;

            while (i > 0 && array[i - 1] >= array[i])
                i--;

            if (i <= 0)
                return false;

            // Find successor to pivot
            var j = array.Length - 1;

            while (array[i - 1] >= array[j])
                j--;

            var temp = array[i - 1];
            array[i - 1] = array[j];
            array[j] = temp;

            // Reverse suffix
            j = array.Length - 1;

            while (i < j)
            {
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }

            return true;
        }
    }
}
