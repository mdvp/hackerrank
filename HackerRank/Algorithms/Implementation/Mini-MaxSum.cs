﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Implementation
{
    class Mini_MaxSum
    {
        static void Main(String[] args)
        {
            var arr_input = Console.ReadLine().Split(' ');
            var input = Array.ConvertAll(arr_input, long.Parse);

            //*** Hard way ***
            //var length = input.Length;

            //var indices = Enumerable.Range(0, length).ToList();
            //var sums = new List<long>();

            //for (var i = 0; i < length; i++)
            //{
            //    sums.Add(input.Where((x, y) => y != i).Sum());
            //}

            //Console.WriteLine(string.Join(" ", sums.Min(), sums.Max()));

            //*** Easy way ***
            var sum = input.Sum();
            var min = input.Min();
            var max = input.Max();

            Console.WriteLine(string.Join(" ", sum - max, sum - min));
        }
    }
}
