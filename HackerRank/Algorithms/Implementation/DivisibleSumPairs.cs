﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Implementation
{
    class DivisibleSumPairs
    {
        static void Main(String[] args)
        {
            string[] tokens_n = Console.ReadLine().Split(' ');
            int n = Convert.ToInt32(tokens_n[0]);
            int k = Convert.ToInt32(tokens_n[1]);
            string[] a_temp = Console.ReadLine().Split(' ');
            int[] a = Array.ConvertAll(a_temp, Int32.Parse);

            var count = 0;
            var counts = new int[k];

            for (var i = 0; i < n; i++)
            {
                var bucket = a[i] % k;
                count += counts[(k - bucket) % k];
                counts[bucket]++;
            }

            Console.WriteLine(count);
        }
    }
}
