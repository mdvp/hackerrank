﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Warmup
{
    class Staircase
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());

            for (var i = 1; i <= n; i++)
            {
                var output = new String('#', i);

                Console.WriteLine(output.PadLeft(n, ' '));
            }
        }
    }
}
