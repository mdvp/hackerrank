﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Warmup
{
    class PlusMinus
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] arr_temp = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll(arr_temp, Int32.Parse);

            var zero = arr.Where(x => x == 0).Count();
            var pos = arr.Where(x => x > 0).Count();
            var neg = arr.Where(x => x < 0).Count();

            Console.WriteLine((Convert.ToDouble(pos) / n).ToString("0.000000"));
            Console.WriteLine((Convert.ToDouble(neg) / n).ToString("0.000000"));
            Console.WriteLine((Convert.ToDouble(zero) / n).ToString("0.000000"));
        }
    }
}
