﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Warmup
{
    class CircularArrayRotation
    {
        static void Main(String[] args)
        {
            /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */

            var tokens = Console.ReadLine().Split(' ');
            var n = Convert.ToInt32(tokens[0]); //length
            var k = Convert.ToInt32(tokens[1]); //loops
            var q = Convert.ToInt32(tokens[2]); //query index

            tokens = Console.ReadLine().Split(' ');

            var arr = Array.ConvertAll(tokens, int.Parse);
            var res = Rotate(arr, n, k);

            for (var i = 0; i < q; i++)
            {
                var query = Convert.ToInt32(Console.ReadLine());

                if (query <= n)
                    Console.WriteLine(res[query]);
            }
        }

        static int[] Rotate(int[] input, int length, int times)
        {
            var res = new int[length];

            for (var i = 0; i < length; i++)
            {
                res[(i + times) % length] = input[i];
            }

            return res;
        }
    }
}
