﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Warmup
{
    class CompareTheTriplets
    {
        static void Main(String[] args)
        {
            string[] tokens_a0 = Console.ReadLine().Split(' ');
            string[] tokens_b0 = Console.ReadLine().Split(' ');

            var alice = 0;
            var bob = 0;

            for (var i = 0; i < 3; i++)
            {
                var ai = Convert.ToInt32(tokens_a0[i]);
                var bi = Convert.ToInt32(tokens_b0[i]);

                if (ai > bi)
                    alice++;
                else if (ai < bi)
                    bob++;
            }

            Console.WriteLine(string.Join(" ", alice, bob));
        }
    }
}
