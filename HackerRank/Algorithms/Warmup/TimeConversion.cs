﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Warmup
{
    class TimeConversion
    {
        static void Main(String[] args)
        {
            string time = Console.ReadLine();

            var token = time.Substring(0, 8);
            var midday = time.Substring(8, 2);

            Console.WriteLine(DateTime.Parse(token + " " + midday).ToString("HH:mm:ss"));
        }
    }
}
