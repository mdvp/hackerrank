﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms.Warmup
{
    class DiagonalDifference
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[][] a = new int[n][];
            for (int a_i = 0; a_i < n; a_i++)
            {
                string[] a_temp = Console.ReadLine().Split(' ');
                a[a_i] = Array.ConvertAll(a_temp, Int32.Parse);
            }

            var pri = 0;
            var sec = 0;

            for (var i = 0; i < n; i++)
            {
                pri += a[i][i];
                sec += a[a.Length - 1 - i][i];
            }

            Console.WriteLine(Math.Abs(pri - sec));
        }
    }
}
