﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HackerRank.Algorithms.Strings
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/determining-dna-health/problem
    /// </summary>
    class DeterminingDNAHealth
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] genes = Console.ReadLine().Split(' ').Take(n).ToArray();
            string[] health_temp = Console.ReadLine().Split(' ').Take(n).ToArray();
            int[] health = Array.ConvertAll(health_temp, Int32.Parse);
            int s = Convert.ToInt32(Console.ReadLine());
            int[] output = new int[s];
            for (int a0 = 0; a0 < s; a0++)
            {
                string[] tokens_first = Console.ReadLine().Split(' ');
                int first = Convert.ToInt32(tokens_first[0]);
                int last = Convert.ToInt32(tokens_first[1]);
                string d = tokens_first[2];
                int total = 0;

                for (int d0 = first; d0 <= last; d0++)
                    total += health[d0] * GetFoundedTimes(d, genes[d0]);

                output[a0] = total;
            }

            Console.WriteLine($"{output.Min()} {output.Max()}");
        }

        static int GetFoundedTimes(string input, string value)
        {
            var result = 0;
            var index = 0;

            while (index < input.Length)
            {
                index = input.IndexOf(value, index);

                if (index == -1)
                    break;

                result++;
                index++;
            }

            return result;
        }
    }
}
