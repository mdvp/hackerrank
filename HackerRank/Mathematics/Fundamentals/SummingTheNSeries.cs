﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Mathematics.Fundamentals
{
    class SummingTheNSeries
    {
        static void Main(String[] args)
        {
            var t = Convert.ToByte(Console.ReadLine());

            for (byte i0 = 0; i0 < t; i0++)
            {
                var n = Convert.ToUInt64(Console.ReadLine());

                //var x = Enumerable.Range(1, n).Sum(aa => Math.Pow(aa, 2) - Math.Pow(aa - 1, 2));

                var tn = 0.0;

                for (ulong i1 = 1; i1 <= n; i1++)
                {
                    var tt = Task.Run(() => { return Math.Pow(i1, 2) - Math.Pow(i1 - 1, 2); });

                    tn += tt.Result;

                    //tn += Math.Pow(i1, 2) - Math.Pow(i1 - 1, 2);


                    //Parallel.For(1L, n, (x) =>
                    //{
                    //    tn += Math.Pow(x, 2) - Math.Pow(x - 1, 2);
                    //});

                }



                //var tn = 0.0;

                //while (n != 0)
                //{
                //    tn += n % 10;
                //    n /= 10;
                //}

                //var x = (n * tn) / 2;

                //var tn = 0.0;

                //for (ulong i1 = 1; i1 <= n; i1++)
                //{
                //    tn += Math.Pow(i1, 2) - Math.Pow(i1 - 1, 2);
                //}

                //while (n != 0)
                //{
                //    tn += n % 10;
                //    n /= 10;
                //}

                Console.WriteLine(tn % (Math.Pow(10, 9) + 7));
            }
        }
    }
}
