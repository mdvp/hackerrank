﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Mathematics.Fundamentals
{
    class LeonardosPrimeFactors
    {
        static void Main(String[] args)
        {
            var q = Convert.ToUInt32(Console.ReadLine());

            for (uint i0 = 0; i0 < q; i0++)
            {
                var n = Convert.ToUInt64(Console.ReadLine());
                var a = new List<ulong>();

                for (ulong i1 = 2; i1 <= n; i1++)
                {
                    if (IsPrime(i1))
                    {
                        if (a.Any() && a.Aggregate(i1, (aa, bb) => aa * bb) > n)
                            break;

                        a.Add(i1);
                    }
                }

                Console.WriteLine(a.Count());
            }
        }

        private static bool IsPrime(ulong input)
        {
            if ((input & 1) == 0)
            {
                if (input == 2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            for (ulong i = 3; (i * i) <= input; i += 2)
            {
                if ((input % i) == 0)
                {
                    return false;
                }
            }

            return input != 1;
        }
    }
}
