﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Mathematics.Fundamentals
{
    class ConnectingTowns
    {
        static void Main(String[] args)
        {
            var t = Convert.ToUInt16(Console.ReadLine());

            for (ushort i = 0; i < t; i++)
            {
                var n = Convert.ToByte(Console.ReadLine());
                var ni = Console.ReadLine().Split(' ').Select(aa => Convert.ToUInt32(aa));

                var x = ni.Aggregate(1u, (aa, bb) => (aa * bb) % 1234567);

                Console.WriteLine(x);
            }
        }
    }
}
