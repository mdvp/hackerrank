﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Mathematics.Fundamentals
{
    class FindThePoint
    {
        static void Main(string[] args)
        {
            var x = Convert.ToByte(Console.ReadLine());

            for (byte i = 0; i < x; i++)
            {
                var input = Console.ReadLine().Split(' ');
                var px = Convert.ToInt16(input[0]);
                var py = Convert.ToInt16(input[1]);
                var qx = Convert.ToInt16(input[2]);
                var qy = Convert.ToInt16(input[3]);

                var rx = qx + (qx - px);
                var ry = qy + (qy - py);

                Console.WriteLine(string.Join(" ", rx, ry));
            }
        }
    }
}
