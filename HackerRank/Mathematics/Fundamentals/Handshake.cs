﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Mathematics.Fundamentals
{
    class Handshake
    {
        static void Main(String[] args)
        {
            var t = Convert.ToUInt16(Console.ReadLine());

            for (ushort i0 = 0; i0 < t; i0++)
            {
                var n = Convert.ToUInt32(Console.ReadLine());
                var x = 0U;

                for (uint i1 = 1; i1 < n; i1++)
                {
                    x += n - i1;
                }

                Console.WriteLine(x);
            }
        }
    }
}
