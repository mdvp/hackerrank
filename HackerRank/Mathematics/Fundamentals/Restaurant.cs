﻿using System;

namespace HackerRank.Mathematics.Fundamentals
{
    class Restaurant
    {
        static void Main(String[] args)
        {
            var t = Convert.ToUInt16(Console.ReadLine());

            for (var i = 0; i < t; i++)
            {
                var input = Console.ReadLine().Split(' ');
                var l = Convert.ToUInt16(input[0]);
                var b = Convert.ToUInt16(input[1]);

                var max_size = gcd(l, b);
                var squares = max_size * max_size;
                var max_squares = l * b;

                Console.WriteLine(max_squares / squares);
            }
        }

        static uint gcd(uint a, uint b)
        {
            while (b != 0)
            {
                var t = b;
                b = a % b;
                a = t;
            }

            return a;
        }
    }
}
