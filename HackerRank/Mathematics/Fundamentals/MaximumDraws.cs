﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Mathematics.Fundamentals
{
    class MaximumDraws
    {
        static void Main(string[] args)
        {
            var t = Convert.ToUInt16(Console.ReadLine());

            for (ushort i = 0; i < t; i++)
            {
                var n = Convert.ToUInt32(Console.ReadLine());

                var x = n + 1;

                Console.WriteLine(x);
            }
        }
    }
}
